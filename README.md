<h1 align="left">ImageDownloader-simple-java</h1>

###

<h3 align="left">About Script:</h3>

###

<p align="left">This script with Java language and Spring framework starts to search in the Google images section through the given API and keyword and depending on the number of photos requested in the configuration file, it finds and downloads them and saves the file address and the downloaded link in the database. <br><br>Configuration file address: <br><br>src\main\resources\application.yml</p>

###

<h3 align="left">usage :</h3>

###

<div align="left">
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/docker/docker-original.svg" height="40" alt="docker logo"  />
</div>

###

<h3 align="left">1 - docker-compose up --build</h3>

###

<p align="left">after build and run docker u have postgreSql on port 5432 and ImageDownloader on port 8081 .</p>

###

<h3 align="left">2 - api request for search and download :</h3>

###

<p align="left">localhost:8081/api/v1/search?q={your query}<br><br><br>for example : <br>localhost:8081/api/v1/search?q=kitten</p>

###

<div align="center">
  <img height="200" src="https://i.ibb.co/NpkgYjT/Screenshot-2024-04-30-044034.png"  />
</div>

###

<p align="left">if u live in iran or china u mustc use proxy.. Some photos may not be downloaded</p>

###

<div align="center">
  <img height="200" src="https://i.ibb.co/0rLWBmR/Screenshot-2024-04-30-043315.png"  />
</div>

###