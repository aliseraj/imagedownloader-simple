package com.aliseraj.imgdl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImgdlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImgdlApplication.class, args);
	}

}
