package com.aliseraj.imgdl.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "stored")
public class Image {
    @Id
    @GeneratedValue
    private Long id;
    private String uuid;
    private String path;
    private String filename;
    private String extension;
    private String link;
    @CreatedDate
    private Date uploaded;
}
