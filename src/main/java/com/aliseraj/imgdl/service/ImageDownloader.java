package com.aliseraj.imgdl.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
@RequiredArgsConstructor
@Service
public class ImageDownloader {
    private final ImageProcess imageProcess;
    private final ImageResize imageResize;
    @Value("${googleKey.apiKey}")
    private String apiKey;

    @Value("${image.maxImages}")
    private int maxImages;

    public void run(String searchQuery) {
        fetchAndStoreImages(searchQuery, maxImages, apiKey);
    }

    @SneakyThrows
    private void fetchAndStoreImages(String searchQuery, int maxImages, String apiKey) {
        JSONArray images;
        try {
            images = imageProcess.fetchImages(searchQuery, apiKey, maxImages);
        } catch (Exception e) {
            log.error("api call error");
            throw e;
        }

        ArrayList<LinkStore> links = new ArrayList<>();
        for (int i = 0; i < images.length(); i++) {
            JSONObject item = images.getJSONObject(i);
            String imageUrl = item.getString("link");
            links.add(new LinkStore(imageUrl,i));
        }

        links.stream()
                .parallel()
                .forEach(linkStore -> {
                    try {
                        BufferedImage img = ImageIO.read(new URL(linkStore.link()));
                        BufferedImage resizedImage = imageResize.resizeImage(img);
                        imageProcess.storeImage(resizedImage, searchQuery,linkStore.link());
                        System.out.println((linkStore.index()) + " " + "Saved image: " + linkStore.link());
                    } catch (Exception e) {
                        log.error("get image error maybe it's because of filtering");
                    }
                });

    }

    private record LinkStore(String link, int index) {
    }

}


