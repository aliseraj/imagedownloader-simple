package com.aliseraj.imgdl.service;

import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;

@Component
public class ImageResize {
    public BufferedImage resizeImage(BufferedImage originalImage) {
        Image image = originalImage.getScaledInstance(800, 500 , Image.SCALE_DEFAULT);
        BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // image to buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(image, 0, 0, null);
        bGr.dispose();

        return bimage;
    }
}
