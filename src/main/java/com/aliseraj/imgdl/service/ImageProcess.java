package com.aliseraj.imgdl.service;


import com.aliseraj.imgdl.model.Image;
import com.aliseraj.imgdl.repository.ImageRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;


@RequiredArgsConstructor
@Service
public class ImageProcess {
    private final ImageRepository imageRepository;
    private final static Path FOLDER = Path.of("./img/");
    private final static String EXTENSOIN = "png";

    public JSONArray fetchImages(String searchQuery, String apiKey, int maxImages) throws IOException, InterruptedException {
        String baseUrl = "https://customsearch.googleapis.com/customsearch/v1";
        String cx = "017901247231445677654:zwad8gw42fj";
        String rights = "(cc_publicdomain|cc_attribute|cc_sharealike|cc_nonderived).-(cc_noncommercial)";
        String encodedRights = URLEncoder.encode(rights, StandardCharsets.UTF_8.toString());
        String encodedQuery = URLEncoder.encode(searchQuery, StandardCharsets.UTF_8.toString());

        String url = String.format("%s?key=%s&cx=%s&searchType=image&num=%d&rights=%s&q=%s",
                baseUrl, apiKey, cx, maxImages, encodedRights, encodedQuery);
//        HttpClient client = HttpClient.newBuilder()
//                .proxy(ProxySelector.of(new InetSocketAddress("127.0.0.1", 2080)))
//                .build();
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url)).build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() != 200) {
            throw new RuntimeException("Failed to fetch images: " + response.body());
        }

        JSONObject jsonResponse = new JSONObject(response.body());

        return jsonResponse.getJSONArray("items");
    }


    public Image storeImage(BufferedImage image, String name,String imageUrl) throws Exception {
        String imageUuid = UUID.randomUUID().toString();

        Image imageEntity = new Image();
        imageEntity.setUuid(imageUuid);
        imageEntity.setPath(FOLDER.toString());
        imageEntity.setExtension(EXTENSOIN);
        imageEntity.setFilename(name);
        imageEntity.setLink(imageUrl);
        Files.createDirectories(FOLDER);
        FileOutputStream fileOutputStream = new FileOutputStream(FOLDER + File.separator + imageUuid + "." + EXTENSOIN);
        ImageIO.write(image, EXTENSOIN, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();

        return imageRepository.save(imageEntity);
    }
}
