package com.aliseraj.imgdl.controller;

import com.aliseraj.imgdl.model.Image;
import com.aliseraj.imgdl.repository.ImageRepository;
import com.aliseraj.imgdl.service.ImageDownloader;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class SearchController {
    private final ImageDownloader imageDownloader;
    private final ImageRepository imageRepository;


    @GetMapping("/search")
    public ResponseEntity<?> search(@RequestParam String q) {
        imageDownloader.run(q);

        return ResponseEntity.ok("Ok");
    }


    @GetMapping("/t")
    public ResponseEntity<?> test(@RequestParam Long id) {
        Optional<Image> image = imageRepository.findById(id);

        return ResponseEntity.ok(image.get());
    }
}
