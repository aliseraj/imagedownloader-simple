package com.aliseraj.imgdl.job;

import com.aliseraj.imgdl.service.ImageDownloader;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

//@Component
@RequiredArgsConstructor
public class RunInStartup {
    private final ImageDownloader imageDownloader;

//    @EventListener
//    public void onApplicationEvent(ContextRefreshedEvent event) {
//        imageDownloader.run();
//    }
}